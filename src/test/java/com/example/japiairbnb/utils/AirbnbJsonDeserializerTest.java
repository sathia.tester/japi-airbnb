package com.example.japiairbnb.utils;

import com.example.japiairbnb.model.json.ListingJson;
import com.example.japiairbnb.model.json.SearchListingJsonEntity;
import org.junit.Before;
import org.junit.Test;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;

public class AirbnbJsonDeserializerTest {

    private SearchListingJsonEntity outputResponse;
    private String inputJson;
    private AirbnbJsonDeserializer deserializer;

    @Before
    public void setup() {
        outputResponse = new SearchListingJsonEntity();
        StringBuilder sb = new StringBuilder();
        try {
            BufferedReader br = new BufferedReader(new FileReader("src/test/resources/outputFromAirbnbRaw.txt"));


            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }

        } catch (FileNotFoundException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }

        inputJson = sb.toString();
        deserializer = new AirbnbJsonDeserializer();
    }

    @Test
    public void testSetFields() {

        deserializer.setFields(outputResponse, inputJson);


    }




}
