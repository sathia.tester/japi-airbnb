package com.example.japiairbnb.utils;

import com.example.japiairbnb.model.json.ListingJson;
import com.example.japiairbnb.model.json.PaginationJson;
import com.example.japiairbnb.model.json.SearchListingJsonEntity;
import com.example.japiairbnb.model.json.TokenJsonEntity;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;

import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

public class AirbnbJsonDeserializer {

    public void setFields(SearchListingJsonEntity searchListingJsonEntity, String content) {

        List<ListingJson> listingList = new ArrayList<>();
        PaginationJson paginationJson = new PaginationJson();

        ObjectMapper m = new ObjectMapper();
        try {
            JsonNode rootNode = m.readTree(content);

            JsonNode searchResultsNode = rootNode.path("search_results");
            if (searchResultsNode.isArray()) {
                for (JsonNode searchResultsItem : searchResultsNode) {
                    JsonNode listingNode = searchResultsItem.path("listing");
                    ListingJson listingJson = new ListingJson();
                    listingJson.setBathrooms(listingNode.path("bathrooms").asDouble());
                    listingJson.setBedrooms(listingNode.path("bedrooms").asInt());
                    listingJson.setBeds(listingNode.path("beds").asInt());
                    listingJson.setCity(listingNode.path("city").asText());
                    listingJson.setId(listingNode.path("id").asInt());
                    listingJson.setInstantBookable(listingNode.path("instant_bookable").asBoolean());
                    listingJson.setNewListing(listingNode.path("is_new_listing").asBoolean());
                    listingJson.setLat(listingNode.path("lat").asDouble());
                    listingJson.setLng(listingNode.path("lng").asDouble());
                    listingJson.setName(listingNode.path("name").asText());
                    listingJson.setNeighborhood(listingNode.path("neighborhood").asText());
                    listingJson.setPictureUrl(listingNode.path("picture_url").asText());
                    listingJson.setPreviewEncodedPng(listingNode.path("preview_encoded_png").asText());
                    listingJson.setPropertyType(listingNode.path("property_type").asText());
                    listingJson.setReviewsCount(listingNode.path("reviews_count").asInt());
                    listingJson.setRoomType(listingNode.path("room_type").asText());
                    listingJson.setStarRating(listingNode.path("star_rating").asDouble());
                    listingJson.setXlPictureUrl(listingNode.path("xl_picture_url").asText());

                    listingList.add(listingJson);
                }
            }

            JsonNode metadataNode = rootNode.path("metadata");
            JsonNode paginationNode = metadataNode.path("pagination");
            int nextOffset = paginationNode.path("next_offset").asInt();
            int resultCount = paginationNode.path("result_count").asInt();
            int listingsCount = metadataNode.path("listings_count").asInt();

            paginationJson.setNext_offset(nextOffset);
            paginationJson.setResultCount(resultCount);
            paginationJson.setListingsCount(listingsCount);

        } catch (IOException e) {
            e.printStackTrace();
        }

        searchListingJsonEntity.setListings(listingList);
        searchListingJsonEntity.setPagination(paginationJson);

    }

    /**
     * Sets fields into token instance
     *
     * @param tokenJsonEntity JSON entity
     * @param content response content
     */

    public void setFields(TokenJsonEntity tokenJsonEntity, String content) {
        ObjectMapper mapper = new ObjectMapper();
        try {
            JsonNode rootNode = mapper.readTree(content);
            tokenJsonEntity.setToken(rootNode.path("access_token").asText());
            tokenJsonEntity.setGenerated(rootNode.path("generated").asText());
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
