package com.example.japiairbnb.utils;

import com.example.japiairbnb.model.AirbnbMainResponse;
import com.example.japiairbnb.model.AirbnbResponseWrapper;
import com.example.japiairbnb.model.AirbnbResponseMessage;
import com.example.japiairbnb.model.SearchListingRequest;

import java.io.*;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;

public class ConnectionHandler {

    private static final String URL_PART1 = "https://api.airbnb.com/v2/search_results";
    private static final String URL_TOKEN_PART1 = "https://api.airbnb.com/v1/authorize";


    public AirbnbResponseWrapper getJsonResponse(SearchListingRequest listingRequest) {

        String locationName = (listingRequest.getLocation() != null) ? listingRequest.getLocation() : "";
        String petsAllowdName = listingRequest.isPetsAllowed() ? "true" : "false";
        String numGuestsName = listingRequest.getNumGuests() == 0 ? Integer.toString(1) : Integer.toString(listingRequest.getNumGuests());
        String numAdultsName = listingRequest.getNumAdults() == 0 ? Integer.toString(1) : Integer.toString(listingRequest.getNumAdults());
        String minPicCountName = Integer.toString(listingRequest.getMinPicCount());
        String minSectionOffsetName = Integer.toString(listingRequest.getOffset());

        String urlName = URL_PART1 + "?location=" + locationName + "&pets_allowed=" + petsAllowdName
                + "&num_guests=" + numGuestsName + "&num_adults=" + numAdultsName
                + "&min_pic_count=" + minPicCountName + "&_offset=" + minSectionOffsetName + "&_limit=20";


        AirbnbMainResponse mainResponse = new AirbnbMainResponse(-1, "Something wrong");

        try {
            URL urlToConnect = new URL(urlName);
            HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();
            connection.setRequestProperty("Host", "api.airbnb.com");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Language", "en-us");
            connection.setRequestProperty("Content-Type", "application/json; charset=UTF-8");
            connection.setRequestProperty("X-Airbnb-API-Key", listingRequest.getAirbnbApiKey());
            connection.setRequestProperty("X-Airbnb-Device-ID", listingRequest.getAirbnbDeviceId());
            connection.setRequestProperty("X-Airbnb-Advertising-ID", listingRequest.getAirbnbAdvertisingId());
            connection.setRequestProperty("X-Airbnb-OAuth-Token", listingRequest.getAirbnbOauthToken());
            connection.setRequestProperty("User-Agent", "Airbnb/15.50 iPhone/9.2 Type/Phone");
            connection.setRequestMethod("GET");

            int responseCode = connection.getResponseCode();

            if(responseCode != 200) {
                AirbnbResponseMessage message = new AirbnbResponseMessage(responseCode, connection.getResponseMessage());
                return message;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            mainResponse = new AirbnbMainResponse(responseCode, sb.toString());
        } catch (MalformedURLException e) {
            e.printStackTrace();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return mainResponse;
    }

    public AirbnbResponseWrapper getJsonResponse(String tokenJsonRequestStr) {

        String urlName = URL_TOKEN_PART1;

        AirbnbMainResponse mainResponse = new AirbnbMainResponse(-1, "Something wrong");
        try {
            URL urlToConnect = new URL(urlName);
            HttpURLConnection connection = (HttpURLConnection) urlToConnect.openConnection();
            connection.setDoOutput(true);
            connection.setDoInput(true);
            connection.setRequestMethod("POST");
            connection.setRequestProperty("Host", "api.airbnb.com");
            connection.setRequestProperty("Accept", "application/json");
            connection.setRequestProperty("Accept-Language", "en-us");
            connection.setRequestProperty("Content-Type", "application/json");
            connection.setRequestProperty("X-Airbnb-Currency", "USD");
            connection.setRequestProperty("X-Airbnb-Locale", "en");
            connection.setRequestProperty("X-Airbnb-API-Key", "915pw2pnf4h1aiguhph5gc5b2");
            connection.setRequestProperty("X-Airbnb-Device-ID", "911EBF1C-7C1D-46D5-A925-2F49ED064C92");
            connection.setRequestProperty("X-Airbnb-Advertising-ID", "a382581f36f1635a78f3d688bf0f99d85ec7e21f");
            connection.setRequestProperty("User-Agent", "Airbnb/15.50 iPhone/9.2 Type/Phone");
            OutputStreamWriter osw = new OutputStreamWriter(connection.getOutputStream());
            osw.write(tokenJsonRequestStr);
            osw.flush();
            osw.close();

            int responseCode = connection.getResponseCode();

            if(responseCode != 200) {
                AirbnbResponseMessage message = new AirbnbResponseMessage(responseCode, connection.getResponseMessage());
                connection.disconnect();
                return message;
            }

            BufferedReader br = new BufferedReader(new InputStreamReader(connection.getInputStream()));
            StringBuffer sb = new StringBuffer();
            String line;
            while ((line = br.readLine()) != null) {
                sb.append(line);
            }
            mainResponse = new AirbnbMainResponse(responseCode, sb.toString());
            connection.disconnect();
        } catch (MalformedURLException e) {
            e.printStackTrace(); // TODO to handle exception
        } catch (IOException e) {
            e.printStackTrace(); // TODO to handle exception
        }

        return mainResponse;
    }
}
