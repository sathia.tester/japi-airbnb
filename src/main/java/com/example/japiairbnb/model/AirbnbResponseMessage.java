package com.example.japiairbnb.model;

public class AirbnbResponseMessage extends AirbnbResponseWrapper {

    private final int code;
    private final String message;

    public AirbnbResponseMessage(int code, String message) {
        this.code = code;
        this.message = message;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getContent() {
        return message;
    }
}
