package com.example.japiairbnb.model.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

import java.util.List;

public class SearchListingJsonEntity extends CommonJsonResponse {
    @JsonProperty(value = "Listings")
    private List<ListingJson> listings;
    @JsonProperty(value = "Pagination")
    private PaginationJson pagination;

    @JsonIgnore
    private int responseCode;

    @Override
    public int getResponseCode() {
        return responseCode;
    }

    public List<ListingJson> getListings() {
        return listings;
    }

    public void setListings(List<ListingJson> listings) {
        this.listings = listings;
    }

    public PaginationJson getPagination() {
        return pagination;
    }

    public void setPagination(PaginationJson pagination) {
        this.pagination = pagination;
    }

    public void setResponseCode(int responseCode) {
        this.responseCode = responseCode;
    }
}
