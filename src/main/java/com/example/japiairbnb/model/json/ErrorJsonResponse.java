package com.example.japiairbnb.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ErrorJsonResponse extends CommonJsonResponse {
    @JsonProperty(value = "error_code")
    private int errorCode;
    private String message;

    public int getErrorCode() {
        return errorCode;
    }


    public void setErrorCode(int errorCode) {
        this.errorCode = errorCode;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    @Override
    public int getResponseCode() {
        return errorCode;
    }
}
