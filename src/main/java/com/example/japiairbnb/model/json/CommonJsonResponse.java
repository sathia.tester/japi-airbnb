package com.example.japiairbnb.model.json;

public abstract class CommonJsonResponse {

    public abstract int getResponseCode();

}
