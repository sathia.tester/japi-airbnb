package com.example.japiairbnb.model.json;

import com.fasterxml.jackson.annotation.JsonProperty;

public class PaginationJson {

    @JsonProperty(value = "next_offset")
    private int next_offset;
    @JsonProperty(value = "result_count")
    private int resultCount;
    @JsonProperty(value = "listings_count")
    private int listingsCount;

    public int getNext_offset() {
        return next_offset;
    }

    public void setNext_offset(int next_offset) {
        this.next_offset = next_offset;
    }

    public int getResultCount() {
        return resultCount;
    }

    public void setResultCount(int resultCount) {
        this.resultCount = resultCount;
    }

    public int getListingsCount() {
        return listingsCount;
    }

    public void setListingsCount(int listingsCount) {
        this.listingsCount = listingsCount;
    }
}
