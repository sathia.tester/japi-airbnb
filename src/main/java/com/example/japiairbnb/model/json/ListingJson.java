package com.example.japiairbnb.model.json;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;

public class ListingJson {
    private int id;
    private String name;
    private String city;
    @JsonProperty(value = "property_type")
    private String propertyType;
    private double lat;
    private double lng;

    @JsonProperty(value = "room_type")
    private String roomType;
    private double bathrooms;
    private int bedrooms;
    private int beds;
    @JsonProperty(value = "instant_bookable")
    private boolean instantBookable;
    @JsonProperty(value = "is_new_listing")
    private boolean isNewListing;

    private String neighborhood;
    @JsonProperty(value = "picture-url")
    private String pictureUrl;
    @JsonProperty(value = "preview_encoded_png")
    private String previewEncodedPng;
    @JsonProperty(value = "reviews_count")
    private int reviewsCount;
    @JsonProperty(value = "star_rating")
    private double starRating;
    @JsonProperty(value = "xl_picture_url")
    private String xlPictureUrl;


    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getCity() {
        return city;
    }

    public void setCity(String city) {
        this.city = city;
    }

    public String getPropertyType() {
        return propertyType;
    }

    public void setPropertyType(String propertyType) {
        this.propertyType = propertyType;
    }

    public double getLat() {
        return lat;
    }

    public void setLat(double lat) {
        this.lat = lat;
    }

    public double getLng() {
        return lng;
    }

    public void setLng(double lng) {
        this.lng = lng;
    }

    public String getRoomType() {
        return roomType;
    }

    public void setRoomType(String roomType) {
        this.roomType = roomType;
    }

    public double getBathrooms() {
        return bathrooms;
    }

    public void setBathrooms(double bathrooms) {
        this.bathrooms = bathrooms;
    }

    public int getBedrooms() {
        return bedrooms;
    }

    public void setBedrooms(int bedrooms) {
        this.bedrooms = bedrooms;
    }

    public int getBeds() {
        return beds;
    }

    public void setBeds(int beds) {
        this.beds = beds;
    }

    public boolean isInstantBookable() {
        return instantBookable;
    }

    public void setInstantBookable(boolean instantBookable) {
        this.instantBookable = instantBookable;
    }

    public boolean isNewListing() {
        return isNewListing;
    }

    public void setNewListing(boolean newListing) {
        isNewListing = newListing;
    }

    public String getNeighborhood() {
        return neighborhood;
    }

    public void setNeighborhood(String neighborhood) {
        this.neighborhood = neighborhood;
    }

    public String getPictureUrl() {
        return pictureUrl;
    }

    public void setPictureUrl(String pictureUrl) {
        this.pictureUrl = pictureUrl;
    }

    public String getPreviewEncodedPng() {
        return previewEncodedPng;
    }

    public void setPreviewEncodedPng(String previewEncodedPng) {
        this.previewEncodedPng = previewEncodedPng;
    }

    public int getReviewsCount() {
        return reviewsCount;
    }

    public void setReviewsCount(int reviewsCount) {
        this.reviewsCount = reviewsCount;
    }

    public double getStarRating() {
        return starRating;
    }

    public void setStarRating(double starRating) {
        this.starRating = starRating;
    }

    public String getXlPictureUrl() {
        return xlPictureUrl;
    }

    public void setXlPictureUrl(String xlPictureUrl) {
        this.xlPictureUrl = xlPictureUrl;
    }


}
