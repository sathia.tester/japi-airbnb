package com.example.japiairbnb.model;

public abstract class AirbnbResponseWrapper {

    public abstract int getCode();
    public abstract String getContent();
}
