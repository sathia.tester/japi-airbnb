package com.example.japiairbnb.model;

import com.fasterxml.jackson.annotation.JsonProperty;

public class SearchListingRequest {
    @JsonProperty(value = "airbnb_advertising_id")
    private String airbnbAdvertisingId;
    @JsonProperty(value = "airbnb_api_key")
    private String airbnbApiKey;
    @JsonProperty(value = "airbnb_device_id")
    private String airbnbDeviceId;
    @JsonProperty(value = "airbnb_oauth_token")
    private String airbnbOauthToken;
    private String location;
    @JsonProperty(value = "pets_allowed")
    private boolean petsAllowed;
    @JsonProperty(value = "min_pic_count")
    private int minPicCount;
    @JsonProperty(value = "num_guests")
    private int numGuests;
    @JsonProperty(value = "num_adults")
    private int numAdults;
    @JsonProperty(value = "_offset")
    private int offset;


    public String getAirbnbAdvertisingId() {
        return airbnbAdvertisingId;
    }

    public void setAirbnbAdvertisingId(String airbnbAdvertisingId) {
        this.airbnbAdvertisingId = airbnbAdvertisingId;
    }

    public String getAirbnbApiKey() {
        return airbnbApiKey;
    }

    public void setAirbnbApiKey(String airbnbApiKey) {
        this.airbnbApiKey = airbnbApiKey;
    }

    public String getAirbnbDeviceId() {
        return airbnbDeviceId;
    }

    public void setAirbnbDeviceId(String airbnbDeviceId) {
        this.airbnbDeviceId = airbnbDeviceId;
    }

    public String getAirbnbOauthToken() {
        return airbnbOauthToken;
    }

    public void setAirbnbOauthToken(String airbnbOauthToken) {
        this.airbnbOauthToken = airbnbOauthToken;
    }

    public String getLocation() {
        return location;
    }

    public void setLocation(String location) {
        this.location = location;
    }

    public boolean isPetsAllowed() {
        return petsAllowed;
    }

    public void setPetsAllowed(boolean petsAllowed) {
        this.petsAllowed = petsAllowed;
    }

    public int getMinPicCount() {
        return minPicCount;
    }

    public void setMinPicCount(int minPicCount) {
        this.minPicCount = minPicCount;
    }

    public int getNumGuests() {
        return numGuests;
    }

    public void setNumGuests(int numGuests) {
        this.numGuests = numGuests;
    }

    public int getNumAdults() {
        return numAdults;
    }

    public void setNumAdults(int numAdults) {
        this.numAdults = numAdults;
    }

    public int getOffset() {
        return offset;
    }

    public void setOffset(int offset) {
        this.offset = offset;
    }
}
