package com.example.japiairbnb.model;

public class AirbnbMainResponse extends AirbnbResponseWrapper {

    private final int code;
    private final String content;

    public AirbnbMainResponse(int code, String content) {
        this.code = code;
        this.content = content;
    }

    @Override
    public int getCode() {
        return code;
    }

    @Override
    public String getContent() {
        return content;
    }
}
