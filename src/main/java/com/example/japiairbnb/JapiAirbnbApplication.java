package com.example.japiairbnb;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class JapiAirbnbApplication {

    public static void main(String[] args) {
        SpringApplication.run(JapiAirbnbApplication.class, args);
    }
}
