package com.example.japiairbnb.controller;

import com.example.japiairbnb.model.json.CommonJsonResponse;
import com.example.japiairbnb.model.SearchListingRequest;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;


@RestController
public class RequestController {

    @PostMapping("/listing")
    public ResponseEntity<CommonJsonResponse> listingPostRequest(@RequestBody SearchListingRequest listRequest) {

        CommonJsonResponse listResponse = RequestHandler.getInstance().getResponse(listRequest);

        return new ResponseEntity<CommonJsonResponse>(listResponse, HttpStatus.valueOf(listResponse.getResponseCode()));
    }

    @GetMapping("/listing")
    public ResponseEntity<CommonJsonResponse> listingGetRequest(
            @RequestParam(value = "airbnb_advertising_id", required = false) String airbnb_advertising_id,
            @RequestParam(value = "airbnb_api_key", required = false) String airbnb_api_key,
            @RequestParam(value = "airbnb_device_id", required = false) String airbnb_device_id,
            @RequestParam(value = "airbnb_oauth_token") String airbnb_oauth_token,
            @RequestParam(value = "location", required = false) String location,
            @RequestParam(value = "pets_allowed", required = false) Boolean pets_allowed,
            @RequestParam(value = "num_guests", required = false) Integer num_guests,
            @RequestParam(value = "num_adults", required = false) Integer num_adults,
            @RequestParam(value = "min_pic_count", required = false) Integer min_pic_count,
            @RequestParam(value = "_offset", required = false) Integer _offset) {

        SearchListingRequest listRequest = new SearchListingRequest();
        listRequest.setAirbnbAdvertisingId(airbnb_advertising_id != null ? airbnb_advertising_id : "");
        listRequest.setAirbnbApiKey(airbnb_api_key != null ? airbnb_api_key : "");
        listRequest.setAirbnbDeviceId(airbnb_device_id != null ? airbnb_device_id : "");
        listRequest.setAirbnbOauthToken(airbnb_oauth_token != null ? airbnb_oauth_token : "");
        listRequest.setLocation(location != null ? location : "");
        listRequest.setPetsAllowed(pets_allowed != null ? pets_allowed : false);
        listRequest.setNumGuests(num_guests != null ? num_guests : 1);
        listRequest.setNumAdults(num_adults != null ? num_adults : 1);
        listRequest.setMinPicCount(min_pic_count != null ? min_pic_count : 0);
        listRequest.setOffset(_offset != null ? _offset : 0);

        CommonJsonResponse listResponse = RequestHandler.getInstance().getResponse(listRequest);

        return new ResponseEntity<CommonJsonResponse>(listResponse, HttpStatus.valueOf(listResponse.getResponseCode()));
    }

    @GetMapping("/gettoken")
    public ResponseEntity<CommonJsonResponse> getApiToken(
            @RequestParam(value = "username", required = false) String username,
            @RequestParam(value = "password", required = false) String password) {

        CommonJsonResponse token = RequestHandler.getInstance().getToken(username, password);

        return new ResponseEntity<CommonJsonResponse>(token, HttpStatus.valueOf(token.getResponseCode()));
    }

}
