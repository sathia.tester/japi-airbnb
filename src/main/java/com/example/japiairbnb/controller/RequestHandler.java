package com.example.japiairbnb.controller;

import com.example.japiairbnb.model.*;
import com.example.japiairbnb.model.json.*;
import com.example.japiairbnb.utils.AirbnbJsonDeserializer;
import com.example.japiairbnb.utils.ConnectionHandler;

public class RequestHandler {
    private static RequestHandler ourInstance = new RequestHandler();

    public static RequestHandler getInstance() {
        return ourInstance;
    }

    private RequestHandler() {}


    public CommonJsonResponse getResponse(SearchListingRequest listingRequest) {
        CommonJsonResponse response;
        if (!isRequestValid(listingRequest)) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(400);
            errorJsonResponse.setMessage("Airbnb Oauth token is null!");
            response = errorJsonResponse;
            return response;
        }
        response = getResponseFromUrl(listingRequest);
        return response;
    }

    /**
     * Checks if listingRequest contains all necessary fields
     *
     * @param listingRequest request
     * @return true if request is ok
     */
    private boolean isRequestValid(SearchListingRequest listingRequest) {
        // check if the instance contains necessary not null fields
        return !(listingRequest.getAirbnbOauthToken() == null);
    }

    /**
     * Creates and sends request to URL
     *
     * @param listingRequest request
     * @return response
     */
    private CommonJsonResponse getResponseFromUrl(SearchListingRequest listingRequest) {

        ConnectionHandler connectionHandler = new ConnectionHandler();
        AirbnbResponseWrapper response = connectionHandler.getJsonResponse(listingRequest);

        if (response.getCode() != 200) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(response.getCode());
            errorJsonResponse.setMessage(response.getContent());
            return errorJsonResponse;
        }

        SearchListingJsonEntity searchListingJsonEntity = new SearchListingJsonEntity();
        searchListingJsonEntity.setResponseCode(response.getCode());

        AirbnbJsonDeserializer deserializer = new AirbnbJsonDeserializer();
        deserializer.setFields(searchListingJsonEntity, response.getContent());

        return searchListingJsonEntity;
    }


    public CommonJsonResponse getToken(String username, String password) {
        CommonJsonResponse tokenResponse;
        if (!isTokenRequestValid(username, password)) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(400);
            errorJsonResponse.setMessage("Username and/or password is null!");
            tokenResponse = errorJsonResponse;
            return tokenResponse;
        }
        String tokenJsonRequestStr = "{\"username\":\"" + username  + "\","
                + "\"password\":\"" + password + "\","
                + "\"prevent_account_creation\": true}";

        tokenResponse = getResponseFromUrl(tokenJsonRequestStr);
        return tokenResponse;
    }

    /**
     * Creates and sends request to URL
     *
     * @param tokenJsonRequestStr request for token in JSON format
     * @return response from URL
     */
    private CommonJsonResponse getResponseFromUrl(String tokenJsonRequestStr) {
        ConnectionHandler connectionHandler = new ConnectionHandler();
        AirbnbResponseWrapper response = connectionHandler.getJsonResponse(tokenJsonRequestStr);
        if (response.getCode() != 200) {
            ErrorJsonResponse errorJsonResponse = new ErrorJsonResponse();
            errorJsonResponse.setErrorCode(response.getCode());
            errorJsonResponse.setMessage(response.getContent());
            return errorJsonResponse;
        }

        TokenJsonEntity tokenJsonEntity = new TokenJsonEntity();
        tokenJsonEntity.setResponseCode(response.getCode());

        AirbnbJsonDeserializer deserializer = new AirbnbJsonDeserializer();
        deserializer.setFields(tokenJsonEntity, response.getContent());

        return tokenJsonEntity;
    }

    /**
     * Checks if tokenRequest contains all necessary fields. The fields should not be null and empty
     *
     * @param username username
     * @param password password
     * @return true if request is ok
     */
    private boolean isTokenRequestValid(String username, String password) {
        return (username != null && !username.isEmpty())
                && (password != null && !password.isEmpty());
    }

}
